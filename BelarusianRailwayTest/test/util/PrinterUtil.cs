﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace BelarusianRailwayTest
{
    class PrinterUtil
    {
        public static void printNewsResults(IList<IWebElement> list)
        {
            Console.WriteLine("Результаты поиска: \n");
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine((i + 1) + ". " + list[i].Text);
            }
        }

        public static void printTrainAndTimeResults(IList<IWebElement> listOne, IList<IWebElement> listTwo)
        {
            Console.WriteLine("Результаты: \n");
            for (int i = 0; i < listOne.Count; i++)
            {
                Console.WriteLine((i + 1) + ". \"" + listOne[i].Text
                        + "\" - " + listTwo[i].Text);
            }
        }
    }
}
