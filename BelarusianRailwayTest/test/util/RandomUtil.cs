﻿using System;
using System.Text;

namespace BelarusianRailwayTest
{
    class RandomUtil
    {
        private static readonly string ALPHABET 
            = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public static string GetRandomString(int length)
        {
            Random r = new Random();
            StringBuilder randomString = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                randomString.Append(ALPHABET[r.Next(ALPHABET.Length)]);
            }

            return randomString.ToString();
        }
    }
}
