﻿namespace BelarusianRailwayTest
{
    class GlobalConstants
    {
        public static readonly string GOOGLE_URL = "http://google.com";
        public static readonly string FIRST_PART_OF_RANDOM_SEARCH_URL = "https://www.rw.by/search/?s=Y&q=";
        public static readonly string RW_SITE_URL = "https://www.rw.by/ru/";

        public static readonly string GOOGLE_TITLE = "Google";
        public static readonly string SITE_TITLE = "Официальный сайт - Белорусская железная дорога";
        public static readonly string GOOGLE_SEARCH_TEXT = "белорусская железная дорога";
        public static readonly string BOTTOM_RW_SITE_TEXT = "© 2020 Belarusian Railway";
        public static readonly string EXPECTED_ERROR_TEXT = "К сожалению, на ваш поисковый запрос ничего не найдено.";

        public static readonly int AMOUNT_OF_NEWS = 4;
        public static readonly int RANDOM_STRING_LENGTH = 20;

        public static readonly string[] BUTTONS_NAMES = {
            "Press Center"
            , "Timetable"
            , "Freight"
            , "Passenger Services"
            , "Corporate"
        };
        public static readonly int buttonsNamesAmount = BUTTONS_NAMES.Length;
    }
}
