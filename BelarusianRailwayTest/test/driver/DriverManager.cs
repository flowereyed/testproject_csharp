﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace BelarusianRailwayTest
{
    class DriverManager
    {
        private static IWebDriver driver;

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = new ChromeDriver();
            }
            return driver;
        }

        public static void TearDown()
        {
            if (driver != null)
            {
                try
                {
                    driver.Close();
                    driver.Quit();
                }
                catch
                {
                    Console.WriteLine("Cannot kill browser.");
                }
                finally
                {
                    driver = null;
                }
            }
        }
    }
}