using NUnit.Framework;

namespace BelarusianRailwayTest
{
    [TestFixture]
    public class Tests
    {
        [TearDown]
        public void Quit()
        {
            DriverManager.TearDown();
        }

        [Test]
        public void OpenFromGoogleTest()
        {
            GoogleSearchPage googleSearchPage;
            RwSitePage rwSitePage;
            
            googleSearchPage = new GoogleSearchPage(DriverManager.GetInstance());
            googleSearchPage.MaximizeWindow();
            googleSearchPage.GetPageUrl(GlobalConstants.GOOGLE_URL);
            Assert.AreEqual(googleSearchPage.GetPageTitle(), GlobalConstants.GOOGLE_TITLE);

            googleSearchPage.SetSearchTab(GlobalConstants.GOOGLE_SEARCH_TEXT);
            googleSearchPage.SubmitSearchTab();
            googleSearchPage.WaitAndClickUrl();

            rwSitePage = new RwSitePage(DriverManager.GetInstance());
            rwSitePage.WaitUntilPageLoading();
            Assert.AreEqual(googleSearchPage.GetPageTitle(), GlobalConstants.SITE_TITLE);
        }

        [Test]
        public void WorkWithMainPageTest()
        {
            RwSitePage rwSitePage;
            RwEnPage rwEnPage;

            rwSitePage = new RwSitePage(DriverManager.GetInstance());
            rwSitePage.MaximizeWindow();
            rwSitePage.GetPageUrl(GlobalConstants.RW_SITE_URL);
            Assert.AreEqual(rwSitePage.GetPageTitle(), GlobalConstants.SITE_TITLE);

            rwSitePage.WaitForVisibilityOfLanguages();
            rwSitePage.ClickOnLanguages();
            rwSitePage.WaitForVisibilityOfEnLanguage();
            rwSitePage.ClickOnEnLanguage();

            rwEnPage = new RwEnPage(DriverManager.GetInstance());
            Assert.True(rwEnPage.WaitAndGetAmountOfNews() >= GlobalConstants.AMOUNT_OF_NEWS);
            Assert.True(rwEnPage.WaitAndGetBottomText().Contains(GlobalConstants.BOTTOM_RW_SITE_TEXT));
            Assert.True(rwEnPage.WaitAndGetButtons().Count.Equals(GlobalConstants.buttonsNamesAmount));

            for (int i = 0; i < rwEnPage.WaitAndGetButtons().Count; i++)
            {
                Assert.True(GlobalConstants.BUTTONS_NAMES[i].ToLower()
                    .Equals(rwEnPage.WaitAndGetButtons()[i].Text.ToLower()));
            }
        }
        
        [Test]
        public void WorkWithSearch()
        {
            RwSitePage rwSitePage;
            RwSearchPage rwSearchPage;

            rwSitePage = new RwSitePage(DriverManager.GetInstance());
            rwSitePage.MaximizeWindow();
            rwSitePage.GetPageUrl(GlobalConstants.RW_SITE_URL);
            Assert.AreEqual(rwSitePage.GetPageTitle(), GlobalConstants.SITE_TITLE);

            string randomString = RandomUtil.GetRandomString(GlobalConstants.RANDOM_STRING_LENGTH);
            rwSitePage.SetSearch(randomString);
            rwSitePage.SubmitSearch();

            rwSearchPage = new RwSearchPage(DriverManager.GetInstance());
            string url = GlobalConstants.FIRST_PART_OF_RANDOM_SEARCH_URL + randomString;
            Assert.AreEqual(rwSearchPage.WaitAndGetUrlAddress(randomString), url);
            Assert.AreEqual(rwSearchPage.WaitAndGetErrorText(), GlobalConstants.EXPECTED_ERROR_TEXT);

            SearchedCity searchedCity = new SearchedCity();
            rwSearchPage.ClearSearch();
            rwSearchPage.SetSearch(searchedCity);
            rwSearchPage.ClickOnButton();

            Assert.IsTrue(rwSearchPage.WaitAndGetResults().Count
                    == searchedCity.GetAmountOfSearchedCityResults());

            PrinterUtil.printNewsResults(rwSearchPage.WaitAndGetResults());
        }
    }
}