﻿namespace BelarusianRailwayTest
{
    public class SearchedCity
    {
        private static readonly string SEARCHED_CITY = "Санкт-Петербург";
        private static readonly int AMOUNT_OF_SEARCH_RESULTS = 15;

        public string GetSearchedCity()
        {
            return SEARCHED_CITY;
        }
        
        public int GetAmountOfSearchedCityResults()
        {
            return AMOUNT_OF_SEARCH_RESULTS;
        }
    }
}