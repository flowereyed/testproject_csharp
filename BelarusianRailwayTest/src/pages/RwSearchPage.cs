﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace BelarusianRailwayTest
{
    class RwSearchPage : BasePage
    {
        private const string noteTextLocator = "notetext";
        private const string bigSearchTabLocator = "searchinpm";
        private const string buttonLocator = "//input[@name='s' and @type='submit']";
        private const string searchResultsLocator = "name";

        [FindsBy(How = How.ClassName, Using = noteTextLocator)]
        private IWebElement NoteText { get; set; }

        [FindsBy(How = How.Id, Using = bigSearchTabLocator)]
        private IWebElement BigSearchTab { get; set; }

        [FindsBy(How = How.XPath, Using = buttonLocator)]
        private IWebElement Button { get; set; }

        [FindsBy(How = How.ClassName, Using = searchResultsLocator)]
        private IList<IWebElement> SearchResults { get; set; }

        public RwSearchPage(IWebDriver driver) : base(driver)
        {
        }

        public string WaitAndGetUrlAddress(string keywords)
        {
            WaitUntilUrlContains(keywords);
            return driver.Url;
        }

        public string WaitAndGetErrorText()
        {
            WaitForVisibilityOfElement(By.ClassName(noteTextLocator));
            return NoteText.Text;
        }

        public void ClearSearch()
        {
            BigSearchTab.Clear();
        }

        public void SetSearch(SearchedCity searchedCity)
        {
            BigSearchTab.SendKeys(searchedCity.GetSearchedCity());
        }

        public void ClickOnButton()
        {
            Button.Click();
        }

        public IList<IWebElement> WaitAndGetResults()
        {
            WaitForVisibilityOfElement(By.ClassName(searchResultsLocator));
            return SearchResults;
        }
    }
}