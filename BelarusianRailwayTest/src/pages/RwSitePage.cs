﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BelarusianRailwayTest
{
    class RwSitePage : BasePage
    {
        private const string siteLangLocator = "langText";
        private const string enLangLocator = "//a[@href='/en/']";
        private const string searchTabLocator = "q";

        [FindsBy(How = How.ClassName, Using = siteLangLocator)]
        private IWebElement SiteLang { get; set; }

        [FindsBy(How = How.XPath, Using = enLangLocator)]
        private IWebElement EnLang { get; set; }

        [FindsBy(How = How.Name, Using = searchTabLocator)]
        private IWebElement SearchTab { get; set; }

        public RwSitePage(IWebDriver driver) : base(driver)
        {
        }

        public void WaitUntilPageLoading()
        {
            WaitUntilUrlContains("rw.by");
        }

        public void WaitForVisibilityOfEnLanguage()
        {
            WaitForVisibilityOfElement(By.XPath(enLangLocator));
        }

        public void WaitForVisibilityOfLanguages()
        {
            WaitForVisibilityOfElement(By.ClassName(siteLangLocator));
        }

        public void ClickOnLanguages()
        {
            SiteLang.Click();
        }

        public void ClickOnEnLanguage()
        {
            EnLang.Click();
        }

        public void SetSearch(string str)
        {
            SearchTab.SendKeys(str);
        }

        public void SubmitSearch()
        {
            SearchTab.Submit();
        }
    }
}
