﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;
using System;

namespace BelarusianRailwayTest
{
    class BasePage
    {
        protected IWebDriver driver;
        protected WebDriverWait wait;

        private static readonly int WAIT_FOR_ELEMENT_SECONDS = 20;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(WAIT_FOR_ELEMENT_SECONDS));
            PageFactory.InitElements(driver, this);
        }

        public void MaximizeWindow()
        {
            driver.Manage().Window.Maximize();
        }

        public string GetPageTitle()
        {
            return driver.Title;
        }

        public void GetPageUrl(string url)
        {
            driver.Url = url;
        }

        public void WaitUntilUrlContains(string keywords)
        {
            wait.Until(ExpectedConditions.UrlContains(keywords));
        }

        public void WaitForVisibilityOfElement(By locator)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        public void WaitForVisibilityOfAllWebElements(By locator)
        {
            wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
        }
    }
}
