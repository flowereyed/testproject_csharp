﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace BelarusianRailwayTest
{
    class GoogleSearchPage : BasePage
    {
        private const string rwSiteLocator = "LC20lb";
        private const string searchTabLocator = "q";

        [FindsBy(How = How.ClassName, Using = rwSiteLocator)]
        private IWebElement RwSite { get; set; }

        [FindsBy(How = How.Name, Using = searchTabLocator)]
        private IWebElement SearchTab { get; set; }

        public GoogleSearchPage(IWebDriver driver) : base(driver)
        {
        }

        public void SetSearchTab(string keys)
        {
            SearchTab.SendKeys(keys);
        }

        public void SubmitSearchTab()
        {
            SearchTab.Submit();
        }

        public void WaitAndClickUrl()
        {
            WaitForVisibilityOfElement(By.ClassName(rwSiteLocator));
            RwSite.Click();
        }
    }
}
