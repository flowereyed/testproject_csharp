﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System.Collections.Generic;

namespace BelarusianRailwayTest
{
    class RwEnPage : BasePage
    {
        private const string bottomTextLocator = "copyright";
        private const string allNewsLocator = "//dt[starts-with(@id, 'bx_1373509569')]";
        private const string buttonsPathLocator = "//*[@class=' ' or @class=' last']//b";

        [FindsBy(How = How.ClassName, Using = bottomTextLocator)]
        private IWebElement BottomText { get; set; }

        [FindsBy(How = How.XPath, Using = allNewsLocator)]
        private IList<IWebElement> AllNews { get; set; }

        [FindsBy(How = How.XPath, Using = buttonsPathLocator)]
        private IList<IWebElement> ButtonsPath { get; set; }

        public RwEnPage(IWebDriver driver) : base(driver)
        {
        }

        public int WaitAndGetAmountOfNews()
        {
            WaitForVisibilityOfElement(By.XPath(allNewsLocator));
            return AllNews.Count;
        }

        public string WaitAndGetBottomText()
        {
            WaitForVisibilityOfElement(By.ClassName(bottomTextLocator));
            return BottomText.Text;
        }

        public IList<IWebElement> WaitAndGetButtons()
        {
            WaitForVisibilityOfAllWebElements(By.XPath(buttonsPathLocator));
            return ButtonsPath;
        }
    }
}
